from marcelocg/phoenix

ENV MIX_ENV prod

RUN mkdir -p /app
WORKDIR /app

# Build dependencies
ADD ./mix.exs .
RUN mix deps.get --only prod
RUN mix deps.compile

# Build app
# (add & compile `app` in 2nd step to allow caching of deps if `mix.exs` not changed)
ADD . .
RUN mix compile

# Build static assets
RUN mix phx.digest


#RUN MIX_ENV=test mix compile
#RUN npm install

#CMD ["mix", "phoenix.server"]
