# Lights - Orchestrator - Elixir

This projects uses the Python Lights Wrapper to orchestrate my light system.













# Notes

* Instead of turning `on` we could simply `dim` to desired **intensity**
  * **Dimming** actually automatically turns on the light
  * Also, there is no point setting the `dim` level of a light if it's not turned on
    * We can just set it when turning it on with the `dim` command
    
* Not sure where to do the `id` <> `name` conversion.
  * I think the `LightOrchestrator` should only have the ids.
    --> `ScenarioParser` will take care of mapping the ids
    --> If later we need to controll single lights from the `LightOrchestrator` we can introduce a `LightIds` mapper.



## Todo

* Add a way to prevent using `on: false` an `dimmer` for a light in a scenario config.
  * Implement that when reading from scenarios from file
