defmodule LightOrchestrator.SandboxTest do
  use ExUnit.Case
  alias LightOrchestrator.Sandbox

  test "Run main function in Sandbox" do
    Sandbox.do_stuff()
  end
end
