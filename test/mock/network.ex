defmodule LightOrchestrator.Mock.Network do
  @behaviour LightOrchestrator.Network

  def start_mock do
    Agent.start_link(fn -> [] end, name: __MODULE__)
  end

  def send(cmd) do
    Agent.update(__MODULE__, fn sent_cmds -> sent_cmds ++ [cmd] end)
  end

  def sent_cmds do
    Agent.get(__MODULE__, &(&1))
  end


end
