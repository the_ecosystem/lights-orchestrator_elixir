defmodule LightOrchestratorTest do
  alias LightOrchestrator.Mock
  use ExUnit.Case

  @lights [
    pole_1:    65540,
    pole_2:    65539,
    desk_top:  65538,
    desk_desk: 65537,
    bed_1:     65541,
    bed_2:     65542
  ]

  setup do
    {:ok, _} = Mock.Network.start_mock()
    :ok
  end

  # TODO:
  # test "Light id not found !! (Add test in scenario parsing)"
  # test "Can not use 'dimmer' and 'on: false' on the same command !! (Add test in scenario parsing)"

  describe "Scenario formatting" do

    test "doesn't exist => :error" do
      res = LightOrchestrator.scenario "doesnt exist"
      assert res == {:error, "Scenario doesn't exist"}
    end

    test "ignore case" do
      assert :ok == LightOrchestrator.scenario "bEd"
    end

    test "trim spaces" do
      assert :ok == LightOrchestrator.scenario "   bed   "
    end

  end

  test "'half' sets all lights to 50% w/o changing any other settings" do
    # Scenario half is successful
    assert :ok == LightOrchestrator.scenario "half"

    # All lights have been updated to 50%
    assert_cmd_sent dim_cmd(@lights[:pole_1],    50)
    assert_cmd_sent dim_cmd(@lights[:pole_2],    50)
    assert_cmd_sent dim_cmd(@lights[:desk_top],  50)
    assert_cmd_sent dim_cmd(@lights[:desk_desk], 50)
    assert_cmd_sent dim_cmd(@lights[:bed_1],     50)
    assert_cmd_sent dim_cmd(@lights[:bed_2],     50)
  end

  test "'bed'(scenario) sets all light to off except 'bed_2'(light)" do
    # Scenario is successful
    assert :ok == LightOrchestrator.scenario "bed"

    # All lights have been updated to 50%
    assert_cmd_sent off_cmd(@lights[:pole_1])
    assert_cmd_sent off_cmd(@lights[:pole_2])
    assert_cmd_sent off_cmd(@lights[:desk_top])
    assert_cmd_sent off_cmd(@lights[:desk_desk])
    assert_cmd_sent off_cmd(@lights[:bed_1])
    # Bed 2 - 10% and 'warm'
    assert_cmd_sent   dim_cmd(@lights[:bed_2], 10)
    assert_cmd_sent color_cmd(@lights[:bed_2], "warm")
  end

  test "'Test' scenario uses all available commands" do
    assert :ok == LightOrchestrator.scenario "test"

    # Pole off
    assert_cmd_sent off_cmd(@lights[:pole_1])
    assert_cmd_sent off_cmd(@lights[:pole_2])

    # Desk desk - 70% and 'cold'
    assert_cmd_sent   dim_cmd(@lights[:desk_desk], 70)
    assert_cmd_sent color_cmd(@lights[:desk_desk], "cold")

    # Other - 100% and 'normal'
    assert_cmd_sent   dim_cmd(@lights[:bed_1],    100)
    assert_cmd_sent   dim_cmd(@lights[:bed_2],    100)
    assert_cmd_sent   dim_cmd(@lights[:desk_top], 100)
    assert_cmd_sent color_cmd(@lights[:bed_1],    "normal")
    assert_cmd_sent color_cmd(@lights[:bed_2],    "normal")
    assert_cmd_sent color_cmd(@lights[:desk_top], "normal")
  end

  defp assert_cmd_sent(cmd) do
    assert Enum.member?(Mock.Network.sent_cmds, cmd), "Command not sent: #{inspect(cmd)}"
  end

  defp color_cmd(id, color), do: %{id: id, cmd: "color", arg: color}
  defp dim_cmd(id, val), do: %{id: id, cmd: "dimmer", arg: val}
  defp off_cmd(id), do: %{id: id, cmd: "on", arg: false}

end
