# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :light_orchestrator, LightOrchestratorWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "rkCF/NmTxLlxfkG8PnZ/Xy33usbaC1C3j0MVL9tgVa9q+cVAvWniJoihnIN0Ib1Y",
  render_errors: [view: LightOrchestratorWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: LightOrchestrator.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Depenency Injection config
config :light_orchestrator,
  network_module: LightOrchestrator.Network.PythonWrapper

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
