use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :light_orchestrator, LightOrchestratorWeb.Endpoint,
  http: [port: 4001],
  server: false

# Override DI config with Mocks
config :light_orchestrator,
  network_module: LightOrchestrator.Mock.Network

# Print only warnings and errors during test
config :logger, level: :warn
