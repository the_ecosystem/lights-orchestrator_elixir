#!/bin/sh

PORT=2001
SLEEP=2

docker-compose up --build --timeout 0 -d
sleep $SLEEP
curl -s the-ecosystem.xyz:$PORT
docker-compose logs -f
