defmodule LightOrchestratorWeb.SandboxView do
  use LightOrchestratorWeb, :view


  def render("response.json", %{response: resp}) do
    %{response: resp}
  end


end
