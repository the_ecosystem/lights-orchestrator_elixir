defmodule LightOrchestratorWeb.SandboxController do
  use LightOrchestratorWeb, :controller
  alias LightOrchestrator.Sandbox

  @moduledoc """
  Http requests in the controller are for the Sandbox/POC only.

  For the real implementation we'll tdd a domain layer.
  """

  def index(conn, _params) do
    resp = Sandbox.do_stuff()
    render conn, "response.json", %{response: resp}
  end

  def run_cmd(conn, %{"cmd" => cmd}) do
    cmd
    |> String.downcase
    |> String.trim
    |> String.to_atom
    |> do_run_cmd(conn)
  end

  def do_run_cmd(:off, conn),    do: run_and_ok conn, &Sandbox.off/0
  def do_run_cmd(:bed, conn),    do: run_and_ok conn, &Sandbox.bed/0
  def do_run_cmd(:on, conn),     do: run_and_ok conn, &Sandbox.on/0
  def do_run_cmd(:full, conn),   do: run_and_ok conn, &Sandbox.full/0
  def do_run_cmd(:half, conn),   do: run_and_ok conn, &Sandbox.half/0
  def do_run_cmd(:ten, conn),    do: run_and_ok conn, &Sandbox.ten/0
  def do_run_cmd(:warm, conn),   do: run_and_ok conn, &Sandbox.warm/0
  def do_run_cmd(:cold, conn),   do: run_and_ok conn, &Sandbox.cold/0
  def do_run_cmd(:normal, conn), do: run_and_ok conn, &Sandbox.normal/0


  defp run_and_ok(conn, function) do
    function.()
    render conn, "response.json", %{response: "ok"}
  end

end
