defmodule LightOrchestratorWeb.PageController do
  use LightOrchestratorWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
