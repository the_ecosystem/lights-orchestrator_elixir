defmodule LightOrchestratorWeb.Router do
  use LightOrchestratorWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", LightOrchestratorWeb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/sandbox", SandboxController, :index

    get "/sandbox/:cmd", SandboxController, :run_cmd
    get "/sandbox/on",  SandboxController, :on
    get "/sandbox/off", SandboxController, :off

    get "/sandbox/full", SandboxController, :full
    get "/sandbox/ten",  SandboxController, :ten
    get "/sandbox/half", SandboxController, :half

    get "/sandbox/warm",   SandboxController, :warm
    get "/sandbox/cold",   SandboxController, :cold
    get "/sandbox/normal", SandboxController, :normal

    get "/sandbox/bed", SandboxController, :bed
  end

  # Other scopes may use custom stacks.
  # scope "/api", LightOrchestratorWeb do
  #   pipe_through :api
  # end
end
