defmodule LightOrchestrator do
  @moduledoc """
  LightOrchestrator keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  @network Application.fetch_env!(:light_orchestrator, :network_module)

  @half [
    %{
      name: "all",
      dimmer: 50
    }
  ]

  @bed [
    %{
      name: "pole_1",
      off: true
    },
    %{
      name: "pole_2",
      off: true
    },
    %{
      name: "desk_top",
      off: true
    },
    %{
      name: "desk_desk",
      off: true
    },
    %{
      name: "bed_1",
      off: true
    },
    %{
      name: "bed_2",
      dimmer: 10,
      color: "warm"
    }
  ]

  @test [
    %{
      name: "pole_1",
      off: true
    },
    %{
      name: "pole_2",
      off: true
    },
    %{
      name: "desk_desk",
      dimmer: 70,
      color: "cold"
    },
    %{
      name: "desk_top",
      dimmer: 100,
      color: "normal"
    },
    %{
      name: "bed_1",
      dimmer: 100,
      color: "normal"
    },
    %{
      name: "bed_2",
      dimmer: 100,
      color: "normal"
    }
  ]


  @scenarios %{
    half: @half,
    bed: @bed,
    test: @test
  }

  @lights [
    pole_1:    65540,
    pole_2:    65539,
    desk_top:  65538,
    desk_desk: 65537,
    bed_1:     65541,
    bed_2:     65542
  ]

  def scenario(scenario) do
    scenario
    |> String.downcase
    |> String.trim
    |> String.to_atom
    |> check_if_exist
    |> do_scenario
  end


  defp check_if_exist(scenario) do
    if Map.has_key?(@scenarios, scenario) do
      {:ok, scenario}
    else
      {:error, "Scenario doesn't exist"}
    end
  end


  defp do_scenario({:ok, scenario}) do
    @scenarios[scenario]
    |> Enum.flat_map(&to_network_cmd_list/1)
    |> Enum.each(&@network.send/1)
  end
  defp do_scenario({:error, error}), do: {:error, error}

  defp to_network_cmd_list(cmd = %{name: "all"}) do
    @lights
    |> Enum.map(&assign_cmd(&1, cmd))
    |> Enum.flat_map(&to_network_cmd_list/1)
  end
  defp to_network_cmd_list(cmd) do
    {cmd, []}
    |> fetch_light_id
    |> append_network_cmd(:dim)
    |> append_network_cmd(:off)
    |> append_network_cmd(:color)
    |> extract_network_cmds
  end

  defp assign_cmd(_light ={name, id}, cmd) do
    cmd
    |> Map.put(:id, id)
    |> Map.put(:name, Atom.to_string(name))
  end

  defp fetch_light_id({%{name: name} = cmd, network_cmds}) do
    id = @lights[String.to_atom(name)] # TODO: Remove OR Cover w/ tests
    {Map.put(cmd, :id, id) , network_cmds}
  end

  defp append_network_cmd({%{id: id, dimmer: val} = cmd, network_cmds}, :dim) do
    {cmd, network_cmds ++ [%{id: id, cmd: "dimmer", arg: val}]}
  end
  defp append_network_cmd({%{id: id, color: color} = cmd, network_cmds}, :color) do
    {cmd, network_cmds ++ [%{id: id, cmd: "color", arg: color}]}
  end
  defp append_network_cmd({%{id: id, off: true} = cmd, network_cmds}, :off) do
    {cmd, network_cmds ++ [%{id: id, cmd: "on", arg: false}]}
  end
  defp append_network_cmd(cmds, _), do: cmds

  defp extract_network_cmds({_, network_cmds}), do: network_cmds

end
