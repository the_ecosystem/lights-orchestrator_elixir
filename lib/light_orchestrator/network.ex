defmodule LightOrchestrator.Network do

  @type command :: %{
    id: number,
    cmd: String.t,
    arg: [any] | any
  }

  @callback send(command) :: :ok | {:error, String.t}

end
