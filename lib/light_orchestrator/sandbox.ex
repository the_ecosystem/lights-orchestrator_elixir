defmodule LightOrchestrator.Sandbox do

  @url "http://the-ecosystem.xyz:5000"
  @tradfri_max_concurency 6

  @lights [
    pole_1:    65540,
    pole_2:    65539,
    desk_top:  65538,
    desk_desk: 65537,
    bed_1:     65541,
    bed_2:     65542
  ]

  ~S"""
  [
    {
      "id": 65540,
      "on": {{turn_on}},
      "dimmer": {{dim}},
      "color": "{{color}}"
    }
  ]
  """

  def do_stuff do



    # status(refresh?=true)

    result = status()
    [
      "Sandbox main function ran",
      %{
        status: "success",
        result: result
      }
    ]
  end





  ######################
  ## Scenarios - TEMP ##
  ######################
  def bed do
    [%{
        id: @lights[:bed_2],
        color: "warm"
     },
     %{
        id: @lights[:bed_2],
        dimmer: 10
     }]
    |> exec_async
  end
  def off,  do: all(fn id -> %{id: id, on: false} end)
  def on,   do: all(fn id -> %{id: id, on: true} end)
  def full, do: all(fn id -> %{id: id, dimmer: 100} end)
  def half, do: all(fn id -> %{id: id, dimmer: 50} end)
  def ten,  do: all(fn id -> %{id: id, dimmer: 10} end)
  def warm,  do: all(fn id -> %{id: id, color: "warm"} end)
  def cold,  do: all(fn id -> %{id: id, color: "cold"} end)
  def normal,  do: all(fn id -> %{id: id, color: "normal"} end)

  def all(id_to_cmd_function) do
    @lights
    |> Keyword.values
    |> Enum.map(id_to_cmd_function)
    |> exec_async
  end


  def status(refresh? \\ false) do
    params = get_params(refresh?)
    @url
    |> HTTPotion.get(query: params)
    |> Map.get(:body)
    |> Poison.decode!
  end
  defp get_params(true),  do: %{refresh: true}
  defp get_params(false), do: %{}



  ###################
  ## HTTP Plumbing ##
  ###################
  defp exec_async(cmds) when is_list(cmds) do
    cmds
    |> Task.async_stream(&exec/1, max_concurrency: @tradfri_max_concurency)
    |> Enum.to_list
  end

  defp exec_sync(cmds) when is_list(cmds) do
    Enum.each(cmds, &exec/1)
  end

  defp exec(cmd) when not is_list(cmd) do
    cmd
    |> Poison.encode!
    |> post
    |> check_response_valid
  end
  defp post(json) do
    HTTPotion.post(
      @url,
      headers: ["Content-Type": "application/json"],
      body: json
    )
  end
  defp check_response_valid(%HTTPotion.Response{}), do: :ok
  defp check_response_valid(%HTTPotion.ErrorResponse{message: error}) do
    raise "Error during HTTP request: #{error}"
  end

end
